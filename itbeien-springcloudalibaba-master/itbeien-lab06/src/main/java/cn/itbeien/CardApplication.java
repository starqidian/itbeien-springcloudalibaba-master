package cn.itbeien;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author beien
 * @date 2024-04-16 20:20
 * Copyright© 2024 beien
 */
@SpringBootApplication
@EnableDiscoveryClient
public class CardApplication {
    public static void main(String[] args) {
        SpringApplication.run(CardApplication.class,args);
    }
}
