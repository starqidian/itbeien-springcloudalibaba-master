package cn.itbeien.controller;

import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * @author beien
 * @date 2024-04-16 20:21
 * Copyright© 2024 beien
 */
@Slf4j
@RestController
public class CardController {
    @GetMapping(value = "/card/{commodityCode}/{count}", produces = "application/json")
    public String cardPay(@PathVariable String commodityCode, @PathVariable int count) {
        log.info("Card Service Begin ... xid: " + RootContext.getXID());
         Random random = new Random();
        if (random.nextBoolean()) {
            throw new RuntimeException("this is a mock Exception");
        }
        return "success";
    }
}
