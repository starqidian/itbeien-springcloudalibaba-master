package cn.itbeien.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author beien
 * @date 2024-04-16 19:42
 * Copyright© 2024 beien
 */
@FeignClient("card-service")
public interface CardService {
    @GetMapping(path = "/card/{commodityCode}/{count}")
    String callCard(@PathVariable("commodityCode") String commodityCode,
                    @PathVariable("count") int count);

}
