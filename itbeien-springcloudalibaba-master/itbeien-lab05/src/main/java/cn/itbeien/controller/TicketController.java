package cn.itbeien.controller;

import cn.itbeien.service.CardService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * @author beien
 * @date 2024-04-16 19:41
 * Copyright© 2024 beien
 */
@RestController
public class TicketController {

    @Autowired
    private CardService cardService;

    @GlobalTransactional(timeoutMills = 100000, name = "cloud-ticket-tx")
    @GetMapping(value = "/seata/feign", produces = "application/json")
    public String feign() {

        String result = cardService.callCard("10001", 2);

        Random random = new Random();
        if (random.nextBoolean()) {
            throw new RuntimeException("this is a mock Exception");
        }

        return result;

    }
}
