# 应用系统集成seata-server流程

![](img/票务系统调用卡系统分布式事务.png)

## 1 数据库表和配置文件准备

1. 创建 UNDO_LOG 表

   ```sql
   -- 注意此处增加唯一索引 ux_undo_log
   CREATE TABLE `undo_log` (
     `id` bigint(20) NOT NULL AUTO_INCREMENT,
     `branch_id` bigint(20) NOT NULL,
     `xid` varchar(100) NOT NULL,
     `context` varchar(128) NOT NULL,
     `rollback_info` longblob NOT NULL,
     `log_status` int(11) NOT NULL,
     `log_created` datetime NOT NULL,
     `log_modified` datetime NOT NULL,
     `ext` varchar(100) DEFAULT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
   ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
   ```

2. 创建业务所需要的数据库表

**订单表和卡账户表**

```sql
DROP TABLE IF EXISTS `order_tbl`;
CREATE TABLE `order_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `commodity_code` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT 0,
  `money` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `account_tbl`;
CREATE TABLE `account_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `money` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

3.创建Nacos data-id: `seata.properties` , Group: `SEATA_GROUP` ,导入 [Nacos 配置](https://github.com/seata/seata/blob/2.0.0/script/config-center/config.txt)

在seata.properties中增加示例中需要的如下[事务群组配置](https://seata.io/zh-cn/docs/user/configurations.html)

```properties
   service.vgroupMapping.ticket-service-tx-group=default
   service.vgroupMapping.card-service-tx-group=default
```

## 2 客户端应用整合seata所需配置信息

###  2.1 application.properties

```properties
spring.application.name=ticket-service
server.port=1808
spring.cloud.nacos.discovery.server-addr=127.0.0.1:3333
spring.cloud.nacos.discovery.username=nacos
spring.cloud.nacos.discovery.password=nacos
spring.cloud.nacos.discovery.namespace=b677ef6c-d021-475b-87b7-0b5fe11ff624
spring.cloud.nacos.discovery.group=dev
spring.main.allow-bean-definition-overriding=true

# 微服务配置中心
spring.cloud.nacos.config.server-addr=127.0.0.1:3333
spring.cloud.nacos.config.namespace=b677ef6c-d021-475b-87b7-0b5fe11ff624
spring.cloud.nacos.config.username=nacos
spring.cloud.nacos.config.password=nacos
#spring.config.import方式导入配置项
spring.config.import[0]=optional:nacos:database.properties?group=datasource
spring.config.import[1]=optional:nacos:system.properties?group=system
```

### 2.2 nacos配置项

- database.properties

```properties
base.config.mdb.hostname=127.0.0.1
base.config.mdb.dbname=seata
base.config.mdb.port=3306
base.config.mdb.username=root
base.config.mdb.password=Rootpwd20171127

spring.datasource.name=ticketDataSource
spring.datasource.type=com.alibaba.druid.pool.DruidDataSource
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://${base.config.mdb.hostname}:${base.config.mdb.port}/${base.config.mdb.dbname}?useSSL=false&serverTimezone=UTC
spring.datasource.username=${base.config.mdb.username}
spring.datasource.password=${base.config.mdb.password}
spring.datasource.druid.max-active=20
spring.datasource.druid.min-idle=2
spring.datasource.druid.initial-size=2

```

- system.properties

```properties
spring.application.name=ticket-service
seata.enabled=true
seata.application-id=${spring.application.name}
seata.tx-service-group=${spring.application.name}-tx-group
seata.config.type=nacos
seata.config.nacos.serverAddr=127.0.0.1:3333
seata.config.nacos.dataId=seata.properties
seata.config.nacos.username=nacos
seata.config.nacos.password=nacos
seata.config.nacos.namespace=b677ef6c-d021-475b-87b7-0b5fe11ff624
seata.config.nacos.group=SEATA_GROUP
seata.registry.type=nacos
seata.registry.nacos.application=seata-server
seata.registry.nacos.server-addr=127.0.0.1:3333
seata.registry.nacos.namespace=b677ef6c-d021-475b-87b7-0b5fe11ff624
seata.registry.nacos.username=nacos
seata.registry.nacos.password=nacos
seata.registry.nacos.group=SEATA_GROUP
```

## 3 客户端应用整合seata所需jar

```xml
<dependencies>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-seata</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
   <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
  <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-loadbalancer</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
        </dependency>

         <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid-spring-boot-starter</artifactId>
            <version>1.2.22</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
          	<version>5.1.49</version>
        </dependency>
  		<dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
        </dependency>
   		<dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
         </dependency>
  		<dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
        </dependency>
    </dependencies>
```

## 4 票务系统服务(ticket-service)

- 事务方法

```java
package cn.itbeien.controller;

import cn.itbeien.service.CardService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author beien
 * @date 2024-04-16 19:41
 * Copyright© 2024 beien
 */
@RestController
public class TicketController {

    @Autowired
    private CardService cardService;

    @GlobalTransactional(timeoutMills = 100000, name = "cloud-ticket-tx")
    @GetMapping(value = "/seata/feign", produces = "application/json")
    public String feign() {

        String result = cardService.callCard("10001", 2);

        return result;

    }
}

```

- openfeign接口

```java
package cn.itbeien.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author beien
 * @date 2024-04-16 19:42
 * Copyright© 2024 beien
 */
@FeignClient("card-service")
public interface CardService {
    @GetMapping(path = "/card/{commodityCode}/{count}")
    String callCard(@PathVariable("commodityCode") String commodityCode,
                    @PathVariable("count") int count);

}

```

## 5 预付卡交易服务(card-service)

### 5.1 nacos配置和application.properties

- nacos

```properties
data-id database.properties
group datasource

data-id system.properties
group system

data-id seata.properties
group SEATA_GROUP
```

- application.properties

```properties
spring.application.name=card-service
server.port=1909
spring.cloud.nacos.discovery.server-addr=127.0.0.1:3333
spring.cloud.nacos.discovery.username=nacos
spring.cloud.nacos.discovery.password=nacos
spring.cloud.nacos.discovery.namespace=b677ef6c-d021-475b-87b7-0b5fe11ff624
spring.cloud.nacos.discovery.group=dev
spring.main.allow-bean-definition-overriding=true

# 微服务配置中心
spring.cloud.nacos.config.server-addr=127.0.0.1:3333
spring.cloud.nacos.config.namespace=b677ef6c-d021-475b-87b7-0b5fe11ff624
spring.cloud.nacos.config.username=nacos
spring.cloud.nacos.config.password=nacos
#spring.config.import方式导入配置项
spring.config.import[0]=optional:nacos:database.properties?group=datasource
spring.config.import[1]=optional:nacos:system.properties?group=system

```

### 5.2 代码

```java
package cn.itbeien.controller;

import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * @author beien
 * @date 2024-04-16 20:21
 * Copyright© 2024 beien
 */
@Slf4j
@RestController
public class CardController {
    @GetMapping(value = "/card/{commodityCode}/{count}", produces = "application/json")
    public String cardPay(@PathVariable String commodityCode, @PathVariable int count) {
        log.info("Card Service Begin ... xid: " + RootContext.getXID());
         Random random = new Random();
        if (random.nextBoolean()) {
            throw new RuntimeException("this is a mock Exception");
        }
        return "success";
    }
}

```

## 6 启动seata-server和nacos

```shell
seata-server.bat -p 8091 -h 127.0.0.1 -m db
startup.cmd -m standalone
```

## 7 测试

```properties
http://localhost:1808/seata/feign
```

