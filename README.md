**一个涵盖Spring Cloud Alibaba微服务解决方案所有技术栈的仓库。包含SpringCloud,SpringBoot2.x,Nacos,Dubbo,分布式限流，分布式事务，SMS，RocketMQ公8大模块。希望朋友小手一抖，右上角来个 Star**



作为一个热爱深夜持续输出源码、文档、视频的源码爱好者，希望大佬能够**一键三连**。

![](https://gitee.com/starqidian/images/raw/master/javabase/%E4%B8%80%E9%94%AE%E4%B8%89%E8%BF%9E.png)

**交流群**

可以添加 我为好友，并拉你进一个交流群，也可以关注视频号和公众号。

![](https://gitee.com/starqidian/images/raw/master/javabase/%E5%85%B3%E4%BA%8E%E6%88%91_01.png)

## 两个仓库

- github:<https://github.com/starqidian/itbeien-springcloudalibaba-master>
- gitee:<https://gitee.com/starqidian/itbeien-springcloudalibaba-master>